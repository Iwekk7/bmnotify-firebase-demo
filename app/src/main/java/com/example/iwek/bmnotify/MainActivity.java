package com.example.iwek.bmnotify;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.iwek.bmnotify.databinding.ActivityMainBinding;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;

public class MainActivity extends AppCompatActivity {

    public ActivityMainBinding binding;
    public RemoteMessage remoteMessage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        FirebaseMessaging.getInstance().subscribeToTopic("news");

        String token = FirebaseInstanceId.getInstance().getToken();
        if (getIntent().getExtras() != null){
            remoteMessage = (RemoteMessage) getIntent().getExtras().get("remoteMessage");
        }

        if (remoteMessage != null) {
            binding.token.setText(token);
            binding.getCollapseKey.setText(remoteMessage.getCollapseKey());
            binding.getFrom.setText(remoteMessage.getFrom());
            binding.getMessageId.setText(remoteMessage.getMessageId());
            binding.getSentTime.setText(String.valueOf((int) remoteMessage.getSentTime()));
            binding.getTo.setText(remoteMessage.getTo());
            binding.getTtl.setText(String.valueOf(remoteMessage.getTtl()));
            binding.getMessageType.setText(remoteMessage.getMessageType());
            binding.remoteMessageGetData.setText(String.valueOf(remoteMessage.getData()));
            binding.getBody.setText(remoteMessage.getNotification().getBody());
            binding.getBodyLocalizationKey.setText(remoteMessage.getNotification().getBodyLocalizationKey());
            binding.getClickAction.setText(remoteMessage.getCollapseKey());
            binding.getColor.setText(remoteMessage.getNotification().getColor());
            binding.getIcon.setText(remoteMessage.getNotification().getIcon());
            binding.getTag.setText(remoteMessage.getNotification().getTag());
            binding.getTitle.setText(remoteMessage.getNotification().getTitle());
            binding.getSound.setText(remoteMessage.getNotification().getSound());
            String titleLocalizationArgs[] = remoteMessage.getNotification().getTitleLocalizationArgs();
            if (titleLocalizationArgs != null) {
                binding.getTitleLocalizationArgs.setText(titleLocalizationArgs[0]);
            }
            String bodyLocalizationArgs[] = remoteMessage.getNotification().getBodyLocalizationArgs();
            if (bodyLocalizationArgs != null) {
                binding.getBodyLocalizationArgs.setText(bodyLocalizationArgs[0]);
            }
            binding.body.setText(remoteMessage.getData().get("value_one"));
            binding.title.setText(remoteMessage.getData().get("value_two"));
        }
    }
}

package com.example.iwek.bmnotify;

/**
 * Created by Iwek on 21.10.2016.
 */

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        sendRegistrationToServer(refreshedToken);
        Log.d("Refreshed token: ", refreshedToken);
    }

    private void sendRegistrationToServer(String token) {

    }
}